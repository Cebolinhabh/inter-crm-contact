import React, { Component } from 'react';
import WebFont from 'webfontloader';
import axios from 'axios';

import '../assets/scss/normalize.scss';
import '../assets/scss/webflow.scss';
import '../assets/scss/banco-inter-seguros.webflow.scss';

import Input from './UI/Input/Input';
import { updateObject, checkValidity } from '../helpers/helpers';

//  Avaliar
WebFont.load({
  google: {
    families: ['Titillium Web:300,400,700', 'sans-serif'],
  },
});

const ENDPOINTS = {
  post: '/formSubmit',
};

class App extends Component {
  state = {
    contactForm: {
      name: {
        elementType: 'input',
        elementClass: 'textfield branco',
        elementConfig: {
          type: 'text',
          placeholder: 'Digite seu nome.',
          mask: false,
        },
        value: '',
        validation: {
          required: true,
          minLength: 3,
        },
        valid: false,
        touched: false,
      },
      email: {
        elementType: 'input',
        elementClass: 'textfield branco',
        elementConfig: {
          type: 'email',
          placeholder: 'Digite seu e-mail',
          mask: false,
        },
        value: '',
        validation: {
          required: true,
          minLength: 3,
          isEmail: true,
        },
        valid: false,
        touched: false,
      },
      phone: {
        elementType: 'input',
        elementClass: 'textfield branco',
        elementConfig: {
          type: 'tel',
          placeholder: 'Digite seu telefone',
          mask: ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
          guide: false,
        },
        value: '',
        validation: {
          required: true,
          isBRPhone: true,
        },
        valid: false,
        touched: false,
      },
      type: {
        elementType: 'select',
        elementClass: 'combobox',
        elementConfig: {
          options: [
            { value: 0, displayValue: 'TIPO DE SEGURO' },
            { value: 'AUTOMÓVEL', displayValue: 'AUTOMÓVEL' },
            { value: 'meu cu', displayValue: 'RESIDENCIAL' },
            { value: 'VIAGEM', displayValue: 'VIAGEM' },
            { value: 'PROTEÇÃO FINANCEIRA', displayValue: 'PROTEÇÃO FINANCEIRA' },
            { value: 'PERDA E ROUBO DO CARTÃO', displayValue: 'PERDA E ROUBO DO CARTÃO' },
          ],
        },
        value: '',
        validation: {
          required: true,
          minLength: 2,
        },
        valid: true,
        touched: false,
      },
    },
    formIsValid: false,
    submitted: {
      error: false,
      success: false,
    },
  }

  onSubmit = (event) => {
    const formData = {};

    for (const formElementIdentifier in this.state.contactForm) {
      formData[formElementIdentifier] = this.state.contactForm[formElementIdentifier].value;
    }

    this.setState({
      submitted: {
        success: true,
      },
    });
    event.preventDefault();
  }

  inputChangedHandler = (event, controlName) => {
    let formIsValid = true;
    const updatedContactForm = updateObject(this.state.contactForm, {
      [controlName]: updateObject(this.state.contactForm[controlName], {
        value: event.target.value,
        valid: checkValidity(event.target.value, this.state.contactForm[controlName].validation),
        touched: true,
      }),
    });

    for (const inputIdentifier in updatedContactForm) {
      formIsValid = updatedContactForm[inputIdentifier].valid && formIsValid;
    }

    this.setState({
      contactForm: updatedContactForm,
      formIsValid,
    });
  }

  render() {
    const { formIsValid, submitted } = this.state;
    const formElementsArray = [];
    const messages = {
      error: submitted.error ?
        <div className="w-form-fail">
          <div>Oops! Algo deu errado ao enviar o formulário. Por favor tente novamente</div>
        </div> : '',
      success: submitted.success ?
        <div className="w-form-done">
          <div>Obrigado! Seu formulário foi enviado aos nossos sistemas!</div>
        </div> : '',
    };
    let form = [];

    for (const key in this.state.contactForm) {
      formElementsArray.push({
        id: key,
        config: this.state.contactForm[key],
      });
    }

    form = formElementsArray.map((formElement) => {
      return (<Input
        key={formElement.id}
        elementType={formElement.config.elementType}
        elementClass={formElement.config.elementClass}
        elementConfig={formElement.config.elementConfig}
        value={formElement.config.value}
        invalid={!formElement.config.valid}
        shouldValidate={formElement.config.validation}
        touched={formElement.config.touched}
        changed={event => this.inputChangedHandler(event, formElement.id)}
      />);
    });

    if (submitted.error) {
      return false;
    }

    return (
      <section id="Contato" className="section-conteudo-3">
        <div className="w-container">
          <h1 className="h1 center bold">ENTRE EM CONTATO</h1>
          <div className="w-row">
            <div className="w-col w-col-6">
              <h2 className="h2">
                <strong className="bold-text">Conta com a gente para tirar todas as suas dúvidas e te ajudar a contratar o seguro perfeito para as suas necessidades e condições.</strong>
              </h2>
            </div>
            <div className="w-col w-col-6">
              <div className="w-form">
                <form id="email-form-3" name="email-form-3" data-name="Email Form 3" onSubmit={this.onSubmit}>
                  {form}
                  <input type="submit" value="Enviar" data-wait="Aguarde um instante..." className="cta formulario2 w-button" disabled={!formIsValid} />
                </form>
                {messages.success}
                {messages.error}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default App;
