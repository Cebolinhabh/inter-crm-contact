import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-text-mask';

const Input = (props) => {
  let inputElement = null;
  const inputClasses = props.elementClass.split(' ');

  if (props.invalid && props.shouldValidate && props.touched) {
    inputClasses.push('invalid');
  }

  switch (props.elementType) {
    case ('input'):
      inputElement = (<MaskedInput
        className={inputClasses.concat('w-input').join(' ')}
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed}
        onBlur={props.blur ? props.blur : null}
      />);
      break;
    case ('select'):
      inputElement = (
        <select
          className={inputClasses.concat('w-input').join(' w-select')}
          value={props.value}
          onChange={props.changed}
        >
          {props.elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (<MaskedInput
        className={inputClasses.join(' ')}
        {...props.elementConfig}
        value={props.value}
        onChange={props.changed}
      />);
  }

  return (
    inputElement
  );
};

Input.propTypes = {
  invalid: PropTypes.bool,
  shouldValidate: PropTypes.shape({}),
  touched: PropTypes.bool,
  elementType: PropTypes.string,
  elementClass: PropTypes.string,
  elementConfig: PropTypes.shape({}),
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  changed: PropTypes.func,
  blur: PropTypes.func,
  options: PropTypes.shape({}),
};

Input.defaultProps = {
  invalid: false,
  shouldValidate: {},
  touched: false,
  elementType: 'input',
  elementClass: '',
  elementConfig: {},
  value: '',
  changed: () => {},
  blur: () => {},
  options: {},
};

export default Input;
