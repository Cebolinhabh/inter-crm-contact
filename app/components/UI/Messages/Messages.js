import React from 'react';
import PropTypes from 'prop-types';

const Messages = props => (
  <div className={props.className} role="alert">
    <a href="#" onClick={props.close} className="close" data-dismiss="alert" aria-label="close" title="close">×</a>{props.text}
  </div>
);

Messages.propTypes = {
  className: PropTypes.string,
  close: PropTypes.func,
  text: PropTypes.string,
};

Messages.defaultProps = {
  className: '',
  close: () => {},
  text: '',
};

export default Messages;
